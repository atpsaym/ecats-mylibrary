# ECats::MyLibrary
[E-Cats Library](http://www.cmsc.co.jp/e-cats/)のMyLibraryの貸出・予約の状況を取得することができます。

ライセンスについては`LICENSE.txt`を参照してください。

## Installation
    $ rake install

## Usage
    require 'ecats/mylibrary'
    client = ECats::MyLibrary.new("YOUR ID", "YOUR PASSWORD", "https://opac.osakafu-u.ac.jp/opac-service")
    puts "現在、#{client.loans.size}冊貸出中です。"

`examples/`以下のサンプルも参照してください。

## Contribute
  * [Issue Tracker](https://bitbucket.org/atpsaym/ecats-mylibrary/issues?status=new&status=open)
  * [Pull Reqest](https://bitbucket.org/atpsaym/ecats-mylibrary/pull-requests)
  * [atpsaym@gmail.com](mailto:atpsaym@gmail.com)
