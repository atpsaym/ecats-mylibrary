# -*- coding: utf-8 -*-
require 'mechanize'
require 'date'
require 'ostruct'
require 'ecats/mylibrary/version'
require 'ecats/mylibrary/book'

class ECats
  class MyLibrary
    USER_AGENT = "OpuLibClient #{VERSION}"

    URL_PATH = {
      loans:        "/srv_odr_stat.php?DISP=re&LANG=1&psp=1",
      reservations: "/srv_odr_stat.php?DISP=rs&LANG=1&psp=1"
    }

    attr_accessor :agent

    def initialize(user_id, password, base_url = "https://opac.osakafu-u.ac.jp/opac-service")
      @user_id = user_id
      @password = password
      @base_url = base_url
      @agent = Mechanize.new{|agent|
        agent.user_agent = USER_AGENT
      }
    end

    def loans
      table = get(URL_PATH[:loans]).at(".srv_result_list table")
      return [] if table.nil?

      return table_to_hashes(table).map{|i|
        i.delete :""
        i.delete :no
        i[:continuance_frequency] = i[:continuance_frequency].to_i
        i[:delay_day] = i[:delay_day].to_i
        i[:delayed?] = i[:delay_day] != 0
        i[:extensible?] = i.delete(:extension).tap{|extension|
          break extension.is_a?(Nokogiri::XML::Element) && extension.name == "input"
        }
        i[:loan_date] = Date.parse(i[:loan_date])
        i[:due_date] = Date.parse(i[:due_date])
        i[:book] = Book.new(
          title_and_authors: i.delete(:title),
          location: i.delete(:location),
          call_no: i.delete(:call_no)
        )
        Loan.new(i)
      }
    end

    def reservations
      table = get(URL_PATH[:reservations]).at(".srv_result_list table")
      return [] if table.nil?

      return table_to_hashes(table).map{|i|
        i.delete :no
        i[:user_comment] = "" if i[:user_comment] == " "
        i[:reservation_date] = Date.parse(i[:reservation_date])
        i[:due_date] = Date.parse(i[:due_date])
        i[:status] = {"準備中" => :preparating, "取置中" => :ready}[i[:status]] || raise("Unknown reservation status: \"#{i[:status].dump}\"")
        i[:cancelable?] = i.delete(:cancel).tap{|extension|
          break extension.is_a?(Nokogiri::XML::Element) && extension.name == "input"
        }
        i[:book] = Book.new(
          title_and_authors: i.delete(:title),
          location: i.delete(:location),
          call_no: i.delete(:call_no)
        )
        Reservation.new(i)
      }
    end

    private

    def get(path)
      login(@agent.get(@base_url + path))
    end

    def table_to_hashes(table)
      head, *body = table.search("tr").map{|tr|
        tr.search("./th | ./td").map{|cell|
          cell.at("input") || cell.text
        }
      }
      head.map!{|text|
        text.gsub(/[[:space:]]/, " ").strip.gsub(".", "")
          .gsub(" ", "_").downcase.to_sym
      }
      body.map{|line|
        Hash[head.zip(line)]
      }
    end

    def login(page)
      return page unless is_login_page?(page)

      form = page.form_with(name: /\A(form|frm1)\z/)
      form["LOGIN_USERID"] = @user_id
      form["LOGIN_PASS"] = @password
      next_page = form.submit

      raise LoginError, "Failed to login" if is_login_page?(next_page)
      return next_page
    end

    def is_login_page?(page)
      page.title =~ /Login/
    end

    class Loan < OpenStruct; end
    class Reservation < OpenStruct; end

    class LoginError < StandardError; end
  end
end
