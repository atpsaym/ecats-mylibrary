require 'ostruct'

class ECats
  class MyLibrary
    class Book < OpenStruct
      def initialize(hash)
        super
        if title_and_authors = hash.delete(:title_and_authors)
          title, subtitle, authors =
            if m = title_and_authors.match(/(.+?)(?: [:=] (.+?))? [\/] (.+)/)
              m.captures
            else
              [title_and_authors]
            end
          self.title    = title
          self.subtitle = subtitle
          self.authors  = authors
        end
      end
    end
  end
end
