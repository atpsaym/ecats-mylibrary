# -*- coding: utf-8 -*-
require 'minitest/unit'
require 'ecats/mylibrary'
require 'pit'

MiniTest::Unit.autorun

class Test < MiniTest::Unit::TestCase
  def setup
    login = Pit.get("opu")
    @client = ECats::MyLibrary.new(login["id"], login["password"])
  end

  def test_login_fail
    @client = ECats::MyLibrary.new("invalid", "invalid")
    assert_raises(ECats::MyLibrary::LoginError){@client.loans}
    assert_raises(ECats::MyLibrary::LoginError){@client.reservations}
  end
end
