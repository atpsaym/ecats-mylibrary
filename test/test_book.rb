# -*- coding: utf-8 -*-
require 'minitest/unit'
require 'ecats/mylibrary/book'

MiniTest::Unit.autorun

class TestBook < MiniTest::Unit::TestCase
  def setup
  end

  def test_initialize
    book = ECats::MyLibrary::Book.new(title_and_authors: "南極点のピアピア動画 / 野尻抱介著")
    assert_equal book.title, "南極点のピアピア動画"
    assert_equal book.subtitle, nil
    assert_equal book.authors, "野尻抱介著"

    book = ECats::MyLibrary::Book.new(title_and_authors: "Rubyベストプラクティス : プロフェッショナルによるコードとテクニック / Gregory Brown著 ; 笹井崇司訳")
    assert_equal book.title, "Rubyベストプラクティス"
    assert_equal book.subtitle, "プロフェッショナルによるコードとテクニック"
    assert_equal book.authors, "Gregory Brown著 ; 笹井崇司訳"

    book = ECats::MyLibrary::Book.new(title_and_authors: "表現のための実践ロイヤル英文法 = The royal English grammar for practical expressiveness / 綿貫陽, マーク・ピーターセン共著")
    assert_equal book.title, "表現のための実践ロイヤル英文法"
    assert_equal book.subtitle, "The royal English grammar for practical expressiveness"
    assert_equal book.authors, "綿貫陽, マーク・ピーターセン共著"
  end
end
