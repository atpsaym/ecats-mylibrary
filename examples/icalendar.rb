# -*- coding: utf-8 -*-
# 返却・取置の期限をiCalendar形式で出力する。

require 'icalendar'
require 'pit'

require 'digest/md5'

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__),  "..", "lib"))
require 'ecats/mylibrary'

include Icalendar

login = Pit.get('opu')
client = ECats::MyLibrary.new(login['id'], login['password'])

cal = Calendar.new
client.loans.each{|loan|
  cal.event{|e|
    e.dtstart       = loan.due_date
    e.dtend         = loan.due_date + 1
    e.summary       = "返却:『#{loan.book.title}』"
    e.description   = "大阪府立大学図書館 『#{loan.book.title}』#{loan.book.authors} の返却期限。（延長#{loan.extensible? ? "可" : "不可"}）"
    e.uid           = "#{Digest::MD5.hexdigest(loan.book.title)}-loan@opu-library-#{login['id']}"
  }
}
client.reservations.each{|r|
  next unless r.status == :ready
  cal.event{|e|
    e.dtstart       = r.due_date
    e.dtend         = r.due_date + 1
    e.summary       = "取置:『#{r.book.title}』"
    e.description   = "大阪府立大学図書館 『#{r.book.title}』#{r.book.authors} の取置期限。"
    e.uid           = "#{Digest::MD5.hexdigest(r.book.title)}-reservation@opu-library-#{login['id']}"
  }
}
cal.publish
puts cal.to_ical
