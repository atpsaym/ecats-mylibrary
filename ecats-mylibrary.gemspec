# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ecats/mylibrary/version'

Gem::Specification.new do |spec|
  spec.name          = "ecats-mylibrary"
  spec.version       = ECats::MyLibrary::VERSION
  spec.authors       = ["atpsaym"]
  spec.email         = ["atpsaym@gmail.com"]
  spec.description   = %q{E-Cats MyLibrary Wrapper}
  spec.summary       = %q{E-Cats MyLibrary Wrapper}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "pit"

  spec.add_runtime_dependency "mechanize"
end
